# Sr Pago Test

Proyecto Prueba de compra de gasolina


El proyecto está realizado con:

Java: 8
Spring: 4.3.0
Hibernate: 4.2.5.Final
Maven 
El método de Base de datos donde lo ejecuté es: MySql 5.7.27
Tiene implementación de lombok 1.18.4


Cuenta con 2 servicios expuestos:

GET     http://localhost:8080/srpago/getSales <p>
POST    http://localhost:8080/srpago/buyGasoline

<p>
Explicación:

Servicio getSales: Muestra todos los registros existentes en la tabla sales.
Servicio buyGasoline: Genera un nuevo registro en la tabla sales, teniendo como parametro el objeto infoRQDTO.

Instalación:

Ejecutar script de creación de base de datos

create database srpago;

Datos de conexión:<br>
usuario de BD: root<br>
password: root<br>
url de conexioconexión: jdbc:mysql://localhost:3306/srpago

Los datos de conexión están en el archivo de propiedades: database.properties

La tabla necesaria (sales) se autogenera al levantar el app en wildfly

Compilación y generación de war

mvn clean install


package com.srpago.test.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.srpago.test.client.GasStationClientImpl;
import com.srpago.test.dto.GasStationDetailDTO;
import com.srpago.test.dto.InfoRQDTO;
import com.srpago.test.dto.ResponseDTO;
import com.srpago.test.dto.Results;
import com.srpago.test.model.Sale;
import com.srpago.test.repository.SalesRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class GasStationServiceImpl {

    @Autowired
    SalesRepository salesRepository;

    @Autowired
    GasStationClientImpl gasStationClient;

    /**
     * Realiza el proceso de compra de gasolina
     * @param infoRQDTO
     * @return
     */
    @Transactional
    public ResponseDTO buyGasoline(InfoRQDTO infoRQDTO){

        log.info("==> Realizando compra de combustible... ");

        ResponseDTO responseDTO = new ResponseDTO();
        
        GasStationDetailDTO gasStationDetailDTO = gasStationClient.getGasStationDetails();
        Results gasDetail = getGasStationDetail(infoRQDTO.getGasStation(), gasStationDetailDTO);
        
        if ( gasDetail != null ){
        	Sale sale = new Sale();
        	BeanUtils.copyProperties( infoRQDTO, sale );
        	log.info(" Entidad a guardar: {} ", sale);
            salesRepository.save(sale);
            responseDTO.setMessage("La compra fue realizada con éxito");
            responseDTO.setSuccess(true);
        }else {
        	responseDTO.setError("Los datos de la gasolinera no fueron encontrados");
        	responseDTO.setMessage("La compra no fue realizada, no fue posible encontrar los datos de la gasolinera");
            responseDTO.setSuccess(false);
        }
        
        return responseDTO;

    }

    /**
     * Obtiene la lista de todos los registros de las ventas realizadas
     * @return
     */
    public List<Sale> getSales(){
        return salesRepository.findAll();
    }


    /**
     * Obtiene el detalle de la gasolinera con base en su ID
     * @param gasStationId
     * @param gasStationDetail
     * @return
     */
    public Results getGasStationDetail(String gasStationId, GasStationDetailDTO gasStationDetail){
    	Optional<Results> result = gasStationDetail.getResults().stream().filter( x -> gasStationId.equals( x.get_id())).findFirst();
        if ( result.isPresent() )
        	return (Results) result.get();
        else
        	return null;
    }

}

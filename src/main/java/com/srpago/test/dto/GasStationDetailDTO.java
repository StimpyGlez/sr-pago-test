package com.srpago.test.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GasStationDetailDTO {

    private List<Results> results;

}

package com.srpago.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Results {

    private String _id;
    private String calle;
    private String rfc;
    private String date_insert;
    private String regular;
    private String colonia;
    private String numeropermiso;
    private String fechaaplicacion;
    private String permisoid;
    private String longitude;
    private String latitude;
    private String premium;
    private String razonsocial;
    private String codigopostal;
    private String dieasel;

}

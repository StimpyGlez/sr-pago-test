package com.srpago.test.dto;

import lombok.Getter;
import lombok.Setter;

import java.security.InvalidParameterException;

@Getter
@Setter
public class InfoRQDTO {

    private String email;
    private String name;
    private String lastName;
    private String cardNumber;
    private Integer expirationDateYear;
    private Integer expirationDateMonth;
    private Integer gasType;
    private Double amount;
    private String gasStation;
    private String sellerName;


    public void isValid() throws InvalidParameterException {

        if ( this.email == null || this.email.isEmpty() ){
            throw new InvalidParameterException("El valor del campo eMail es incorrecto, favor de validar");
        }
        if ( this.name == null || this.name.isEmpty() ){
            throw new InvalidParameterException("El valor del campo Name es incorrecto, favor de validar");
        }
        if ( this.lastName == null || this.lastName.isEmpty() ){
            throw new InvalidParameterException("El valor del campo LastName es incorrecto, favor de validar");
        }
        if ( this.cardNumber == null || this.cardNumber.isEmpty() || this.cardNumber.length() < 16 || this.cardNumber.length() > 16 ){
            throw new InvalidParameterException("El valor del campo cardNumber es incorrecto, favor de validar");
        }
        if ( this.expirationDateYear == null || this.expirationDateYear < 2019 ){
            throw new InvalidParameterException("El valor del campo expirationDateYear es incorrecto, favor de validar");
        }
        if ( this.expirationDateMonth == null || this.expirationDateMonth < 1 || this.expirationDateMonth > 12 ){
            throw new InvalidParameterException("El valor del campo expirationDateMonth es incorrecto, favor de validar");
        }
        if ( this.gasType == null || this.gasType < 1 || this.gasType > 2 ){
            throw new InvalidParameterException("El valor del campo gasType es incorrecto, favor de validar");
        }
        if ( this.amount == null || this.amount < 2 ){
            throw new InvalidParameterException("El valor del campo amount es incorrecto, favor de validar");
        }
        if ( this.gasStation == null || this.gasStation.isEmpty() ){
            throw new InvalidParameterException("El valor del campo gasStation es incorrecto, favor de validar");
        }
        if ( this.sellerName == null || this.sellerName.isEmpty() ){
            throw new InvalidParameterException("El valor del campo sellerName es incorrecto, favor de validar");
        }

    }
}

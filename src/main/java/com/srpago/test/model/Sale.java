package com.srpago.test.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@Table(name = "sales")
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "name")
    private String name;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "cardNumber")
    private String cardNumber;

    @Column(name = "expirationDateYear")
    private Integer expirationDateYear;

    @Column(name = "expirationDateMonth")
    private Integer expirationDateMonth;

    @Column(name = "gasType")
    private Integer gasType;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "gasStation")
    private String gasStation;

    @Column(name = "sellerName")
    private String sellerName;

    @Basic(optional = false)
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;

    @PrePersist
    void onPrePersist() {
        setDataCreated();
    }

    /**
     * Asigna la fecha y hora de creación del registro
     */
    public void setDataCreated() {
        this.dateCreated = new Timestamp(new Date().getTime());
    }

}

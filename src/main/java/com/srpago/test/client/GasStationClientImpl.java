package com.srpago.test.client;

import java.io.BufferedInputStream;

import java.io.*;

import com.google.gson.Gson;
import com.srpago.test.dto.GasStationDetailDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class GasStationClientImpl implements GasStationClient {

    /**
     * Obtiene los datos de las gasolineras realizando la de un servicio REST
     * @return
     */
    public GasStationDetailDTO getGasStationDetails(){

        HttpClient httpClient = new DefaultHttpClient();
        GasStationDetailDTO gasStationDetailDTO = new GasStationDetailDTO();

        try {
            HttpGet httpGetRequest = new HttpGet("https://api.datos.gob.mx/v1/precio.gasolina.publico");
            HttpResponse httpResponse = httpClient.execute(httpGetRequest);

            if ( httpResponse.getStatusLine().getStatusCode() == 200){
                HttpEntity entity = httpResponse.getEntity();
                StringBuilder jsonString = new StringBuilder();

                byte[] buffer = new byte[1024];
                if (entity != null) {
                    InputStream inputStream = entity.getContent();
                    try {
                        int bytesRead = 0;
                        BufferedInputStream bis = new BufferedInputStream(inputStream);
                        while ((bytesRead = bis.read(buffer)) != -1) {
                            jsonString.append(new String(buffer, 0, bytesRead));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try { inputStream.close(); } catch (Exception ignore) {}
                    }
                }

                Gson gson=new Gson();
                gasStationDetailDTO = gson.fromJson(jsonString.toString(), GasStationDetailDTO.class);
            }

        } catch (Exception e) {
            log.error(" Error en la ejecución del servicio de detalle de gasolineras {}", e.getMessage());
            e.printStackTrace();
        } finally {
            httpClient.getConnectionManager().shutdown();
        }

        return gasStationDetailDTO;

    }


}

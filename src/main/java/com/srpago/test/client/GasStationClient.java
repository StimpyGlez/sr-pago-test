package com.srpago.test.client;

import com.srpago.test.dto.GasStationDetailDTO;

public interface GasStationClient {

    GasStationDetailDTO getGasStationDetails();

}

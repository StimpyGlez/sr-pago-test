package com.srpago.test.repository;

import com.srpago.test.model.Sale;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface SalesRepository extends CrudRepository<Sale, Long> {

    List<Sale> findAll();

}

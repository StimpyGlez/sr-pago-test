package com.srpago.test.controller;

import com.srpago.test.dto.InfoRQDTO;
import com.srpago.test.dto.ResponseDTO;
import com.srpago.test.model.Sale;
import com.srpago.test.service.GasStationServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.security.InvalidParameterException;
import java.util.List;

@Slf4j
@RestController
public class GasStationController {

    @Autowired
    GasStationServiceImpl gasolinaService;

    /**
     * Servicio que realiza la compra de gasolina
     * @param infoRQDTO
     * @return
     */
    @RequestMapping(value = "/buyGasoline", method = RequestMethod.POST)
    public ResponseDTO buy(@RequestBody InfoRQDTO infoRQDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        log.info(" ===========>> Ejecutando la compra de gasolina...");
        try{
            infoRQDTO.isValid();
            return gasolinaService.buyGasoline(infoRQDTO);
        } catch (InvalidParameterException e) {
            log.error("Error en valiación:", e);
            responseDTO.setError(e.getMessage());
            responseDTO.setMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
            responseDTO.setSuccess(false);
            return responseDTO;
        } catch (Exception e){
            log.error("Error:", e);
            responseDTO.setError(e.getMessage());
            responseDTO.setMessage(e.getCause().toString());
            responseDTO.setSuccess(false);
            return responseDTO;
        }
    }

    /**
     * Obtiene la lista de todos los regstros de compra de gasolina
     * @return
     */
    @RequestMapping(value = "/getSales", method = RequestMethod.GET)
    public List<Sale> getSales() {
    	log.info(" ===========>> Obteniendo consulta de compras...");
        return gasolinaService.getSales();
    }


}
